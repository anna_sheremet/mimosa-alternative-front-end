$(document).ready(function() {
    /* Scrolling navbar section start */
    let prevPos = window.pageYOffset;

    window.onscroll = function () {
        let currentPos = window.pageYOffset;
        if (currentPos >= 15 * 3) {
            if (prevPos > currentPos) {
                document.getElementById('scroll-nav').style.top = "0";
                document.getElementById('scroll-nav').classList.add("border-bottom-line-white");
                document.getElementById('scroll-nav').classList.add("nav-bg");
                document.getElementById('mimosa').style.color = '#343a40'
                document.getElementById('dashboard-btn').classList.add("btn-outline-secondary")
            } else {
                document.getElementById('scroll-nav').style.top = "-5.1em";
            }
            prevPos = currentPos;
        } else {
            document.getElementById('scroll-nav').classList.remove("border-bottom-line-white");
            document.getElementById('scroll-nav').classList.remove("nav-bg");
            document.getElementById('mimosa').style.color = 'white'
            document.getElementById('dashboard-btn').classList.remove("btn-outline-secondary")
        }
    }
    /* Scrolling navbar section end */
});
